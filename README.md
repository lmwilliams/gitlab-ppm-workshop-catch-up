# GitLab PPM Workshop Catch up

This project is for catching up with the GitLab PPM workshop.  Using group and project backups to get you to the proper place

**Scenario:** 
Join Late? or did not finish the lab - here is how you can get caught up:

**Go to**
https://gitlab.com/lmwilliams/gitlab-ppm-workshop-catch-up

**Download the completed lab file**
**Steps to Complete:**
 1. Go to the root of your group
 2. In you group go to New Subgroup
 3. Select Import tab
 4. Choose a Name
 5. Select the downloaded file
 6. Select Import

**If you are on Lab 3**
 1. Download the Project extract
 2. Select New Project
 3. Select Import tab
 4. Choose a Name 
 5. Select the downloaded file
 6.  Select Import
